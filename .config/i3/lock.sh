#!/bin/bash

# Dependencies :
#   -> Font : Gameshow (available here : https://www.dafont.com/gameshow.font (free to use for personal usage)
#   -> imagemagick (for convert)
#   -> i3lock
#   -> xorg-xset 

scrot /tmp/sc.png
convert /tmp/sc.png -blur 0x5 /tmp/sc.png
convert /tmp/sc.png -fill black -colorize 50% /tmp/sc.png
IM=/tmp/sc.png
TXT='LOCKED_'
convert /tmp/sc.png -font Gameshow -pointsize 150 -fill white -gravity center -annotate +0+0 "$TXT" /tmp/sc.png
i3lock -uei /tmp/sc.png
xset dpms force off
