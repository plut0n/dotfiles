" 
"              NeoVim plut0n ult1m4t3 c0nf1g OP
"
"
" Sections:
"    -> General
"    -> (Neo)VIM UI
"    -> Files, backups and undo
"    -> Text, tab and indent related
"    -> Visual mode
"    -> Useful func leader-binded
"    -> Moving around w/ shortcuts
"    -> Editing mapping (uncomplete)
"    -> Spell check
"    -> Plugin related settings
"    -> Plugin List (using vim-plug)
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Number of lines of history (VIM command) to store
set history=500

" Enable filetype plugins
filetype plugin on
filetype indent on

" Set to auto read when a file is changed from the outside
set autoread

" Mapleader (for shortcut) set to ,
let mapleader = ","

" Fast saving
nmap <leader>w :w!<cr>

" Sudo save (neovim compatible)
nmap <leader>W :w suda://%<cr>

" Quit with ,q so n00b can't get how
nmap <leader>q :q!<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => (Neo)VIM UI
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Avoid garbled characters in Chinese language windows OS
let $LANG='en' 
set langmenu=en
"source $VIMRUNTIME/delmenu.vim
"source $VIMRUNTIME/menu.vim
"
" Enable Wildmenu (autocompletion for vim command, inside statusline)
set wildmenu

" Ignore compiled files (useful when opening all in a directory)
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif

"Display current position (lign/column) in the status line
set ru

" Height of the command bar
set cmdheight=1

" A buffer becomes hidden when it is abandoned (not sure why...)
set hid

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase
"
" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch 

" Don't redraw while executing macros (good performance config)
set lazyredraw 

"" For regular expressions turn magic on
"set magic

"" Show matching brackets when text indicator is over them
"set showmatch 
"" How many tenths of a second to blink when matching brackets
"set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" => Colors and Fonts
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax enable 

" Background color
set background=dark

" Set extra options when running in GUI mode
if has("gui_running")
    set guioptions-=T
    set guioptions-=e
    set t_Co=256
    set guitablabel=%M\ %t
endif

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

"" Use Unix as the standard file type
set ffs=unix,dos,mac


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" => Files, backups and undo
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" => Text, tab and indent related
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces (use tab, pls!)
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters (TODO: adjust to buffer pixel width)
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines


"""""""""""""""""""""""""""""""
"" => Visual mode
"""""""""""""""""""""""""""""""
"" Visual mode pressing * or # searches for the current selection
"" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>


""""""""""""""""""""""""""""""""
"" => Useful func leader-binded
""""""""""""""""""""""""""""""""

" Terminal (vertical/horizontal split)
map <leader>tv :vsplit term://zsh<cr>
map <leader>th :split term://zsh<cr>

" Clear last search highlight
map <leader>c :let @/ = ""<cr>

" Switch CWD to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>:pwd<cr>

" Compile current file using pdflatex
map <leader>pdf :!pdflatex %<cr>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" => Moving around w/ shortcuts
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Map <Space> to / (search) and Ctrl-<Space> to ? (backwards search)
map <space> /
map <c-space> ?

" Allow Esc key to returnn to normal mode while in Terminal buffer
tnoremap <ESC> <C-\><C-n>

" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Ez resize of window
map <C-+> :vertical resize +5
map <C--> :vertical resize -5
map <S-+> :resize +5
map <S--> :resize -5

"" Close the current buffer
map <leader>bd :Bclose<cr>:tabclose<cr>gT

"" Close all the buffers
map <leader>ba :bufdo bd<cr>

map <leader>l :bnext<cr>
map <leader>h :bprevious<cr>

"" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove 
map <leader>t<leader> :tabnext 

" Let 'tl' toggle between this and the last accessed tab
let g:lasttab = 1
nmap <Leader>tl :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()


" Opens a new tab with the current buffer's path
" Super useful when editing files in the same directory
map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" Specify the behavior when switching between buffers 
try
  set switchbuf=useopen,usetab,newtab
  set stal=2
catch
endtry

" Return to last edit position when opening files (You want this!)
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Open new buffer when vsplit/split right/below
set splitbelow
set splitright


"""""""""""""""""""""""""""""""
"" => Status line
"""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

"Using powerline, so no need to show mode (redundant)
set noshowmode
"
" Format the status line
set statusline=\ %F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" => Editing mappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Move a line of text using ALT+[jk] or Command+[jk] on mac
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Spell check
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" => Helper functions
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"" Returns true if paste mode is enabled
function! HasPaste()
    if &paste
        return 'PASTE MODE  '
    endif
    return ''
endfunction


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugin related Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" lightline
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugin List (using vim-plug)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'w0rp/ale'
Plug 'lambdalisue/suda.vim'
Plug 'scrooloose/nerdtree'
call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
