#=================================================#
#                      Zshrc                      #
#                      -----                      #
# Sections:                                       # 
#   -> ZSH General                                #
#   -> ZSH Completion                             #
#   -> X / tty                                    #
#   -> Aliases                                    #
#   -> Rice                                       #
#   -> Lib / Path / Env                           #
#   -> Extra functions                            #
#                                                 #
#=================================================#

#=================================================#
#                   ZSH General                   #
#=================================================#

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=10000
bindkey -v


#=================================================#
#                  ZSH Completion                 #
#=================================================#

zstyle :compinstall filename '/home/plut0n/.zshrc'
zstyle ':completion:*' menu select

autoload -Uz compinit
autoload -U colors && colors
compinit
tput smkx #Needed for some buggy app (e.g. irssi)


#=================================================#
#                     X / tty                     #
#=================================================#

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then exec startx; fi
if [[ $(tty) = /dev/tty2 ]]; then exec tmux; fi
[[ $- != *i* ]] && return


#=================================================#
#                     Aliases                     #
#=================================================#

#   ls w/ color
alias ls='ls --color=auto'
alias la='ls -alsh --colo=auto'

#   X output shortcuts
alias displayport='xrandr --output LVDS1 --auto --output DP1 --auto --right-of LVDS1 && feh --bg-scale ~/Images/Wall/wall40.png'
alias hdmi='xrandr --output LVDS1 --auto --output HDMI1 --auto --right-of LVDS1'
alias dualscreen='xrandr --output LVDS1 --auto --output VGA1 --auto --right-of LVDS1 && feh --bg-scale ~/Images/Wall/wall39.jpg'
alias monitorz='xrandr --output LVDS-1 --off --output VGA-1 --auto --output DP-1 --auto --right-of VGA-1 && feh --bg-scale ~/Images/Wall/wall53.jpg'
alias vgahdmi='xrandr --output LVDS1 --off --output VGA1 --auto --output HDMI1 --auto --right-of VGA1 && feh --bg-scale ~/Images/Wall/wall40.png'

#   Utils & other simple functions
alias myip='curl http://ipecho.net/plain'                           # Print IP
alias listpkg="expac -H M '%m\t%n' | sort -h"                       # List install package per size 
alias listf="du -h | sort -h"                                       # List file in a folder per size
alias chromium="firejail chromium --incognito"                      # Start chromium in sage env.
alias emacs="nvim"                                                  # Lol
alias vim="nvim"                                                    # Neovim is nice   
alias kxc="keepassxc ~/.local/kxc/plut0nDB.kdbx"                    # Keepass shortcut
alias dotfiles="git --git-dir=$HOME/Git/dotfiles/.git/ --work-tree=$HOME"   # Git for dotfiles
alias nmd="sudo systemctl start NetworkManager"

#   Virtual Machine
alias protostar="qemu-system-x86_64 -boot d -cdrom ~/Documents/ISO/exploit-exercises-protostar-2.iso -m 2048 -redir tcp:2222::22"
alias tails='qemu-system-x86_64 ~/Documents/ISO/tails-amd64-3.0.iso'

#   Work related
alias icfo="cd ~/Documents/ICFO/"
alias unibas="cd ~/Documents/UniBas/"
alias gvm="gcloud compute ssh --ssh-flag=\"-Y\" ml-vm"

#   Keyboard layout
alias usk="setxkbmap us altgr-intl"
alias frk="setxkbmap fr"


#=================================================#
#                     Rice                        #  
#=================================================#

# PS1 & fetch
PROMPT="%{$fg_bold[red]%}Πλούτων ➜ %{$fg_bold[cyan]%}%c > $(tput sgr0)"
ufetch

#   Wal (async)
(cat ~/.cache/wal/sequences &)
# Alternative (blocks terminal for 0-3ms)
#cat ~/.cache/wal/sequences
#   Wal (tty)
source ~/.cache/wal/colors-tty.sh

#   Less/man color
export LESS_TERMCAP_mb=$(tput bold; tput setaf 2) # green
export LESS_TERMCAP_md=$(tput bold; tput setaf 6) # cyan
export LESS_TERMCAP_me=$(tput sgr0)
export LESS_TERMCAP_so=$(tput bold; tput setaf 3; tput setab 4) # yellow on blue
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 7) # white
export LESS_TERMCAP_ue=$(tput rmul; tput sgr0)
export LESS_TERMCAP_mr=$(tput rev)
export LESS_TERMCAP_mh=$(tput dim)
export LESS_TERMCAP_ZN=$(tput ssubm)
export LESS_TERMCAP_ZV=$(tput rsubm)
export LESS_TERMCAP_ZO=$(tput ssupm)
export LESS_TERMCAP_ZW=$(tput rsupm)


#=================================================#
#                Lib / Path / Env                 #
#=================================================#

#   Bin Path
export PATH=/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/plut0n/mosek/8/tools/platform//bin:/home/plut0n/.android/sdk/build-tools/28.0.2/:/home/plut0n/.gem/ruby/2.6.0/bin:/home/plut0n/.local/go/bin

#   Lib64
export LD_LIBRARY_PATH=:/usr/lib/
#   Go Lib
export GOPATH=/home/plut0n/.local/go
#   Mosek license and config
export MOSEKPLATFORM=linux64x86
export MOSEKLM_LICENSE_FILE=/home/plut0n/.local/share/mosek/mosek.lic

#   Other env variable
export VISUAL="nvim"
export ATMNT_PATH=/mnt
export MESA_GL_VERSION_OVERRIDE=4.5


#=================================================#
#                 Extra Functions                 #
#=================================================#

function mlvm {
    if [ -z "$1" ]; then
        echo "Usage: mlvm [start|stop|ssh]"
    else
        if [ -f $1 ]; then
            case $1 in
                start)  gcloud compute instances start ml-vm    ;;
                stop)   gcloud compute instances stop ml-vm     ;;
                ssh)    gcloud compute ssh ml-vm                ;;
            esac
        fi
    fi
}

function extract {
    if [ -z "$1" ]; then
        echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    else
        if [ -f $1 ] ; then
            case $1 in
                *.tar.bz2)   tar xvjf ./$1    ;;
                *.tar.gz)    tar xvzf ./$1    ;;
                *.tar.xz)    tar xvJf ./$1    ;;
                *.lzma)      unlzma ./$1      ;;
                *.bz2)       bunzip2 ./$1     ;;
                *.rar)       unrar x -ad ./$1 ;;
                *.gz)        gunzip ./$1      ;;
                *.tar)       tar xvf ./$1     ;;
                *.tbz2)      tar xvjf ./$1    ;;
                *.tgz)       tar xvzf ./$1    ;;
                *.zip)       unzip ./$1       ;;
                *.Z)         uncompress ./$1  ;;
                *.7z)        7z x ./$1        ;;
                *.xz)        unxz ./$1        ;;
                *.exe)       cabextract ./$1  ;;
                *)           echo "extract: '$1' - unknown archive method" ;;
            esac
        else
            echo "$1 - file does not exist"
        fi
    fi
}

function countdown(){
   date1=$((`date +%s` + $1)); 
   while [ "$date1" -ge `date +%s` ]; do 
     echo -ne "$(date -u --date @$(($date1 - `date +%s`)) +%H:%M:%S)\r";
     sleep 0.1
   done
}

# GIF as desktop wallpaper. Require i3, gifview (gifsicle) and xwinwrao (shantz-xwinwrap-bzr).
function gwall {
    if [ -z "$1" ]; then
        echo "Usage: gwall [path/to/file.gif]"
    else
        if [ -f $1 ]; then
            i3-msg "exec --no-startup-id xwinwrap -ni -b -fs -ov -nf -- gifview -w WID $1 -a"
        else
            echo "$1 - file does not exist"
        fi
    fi
}
