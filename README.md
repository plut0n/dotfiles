![alt text][status] ![alt text][license]

[status]: https://img.shields.io/badge/status-rolling-red.svg
[license]: https://img.shields.io/badge/License-BSD--2-blue.svg

# Dotfiles - Plut0n (rolling)

## Overview

Setup currently in used on my (Thinkpad) x230. This setup include:
- [i3-gaps](https://github.com/Airblader/i3)
- [rofi](https://github.com/DaveDavenport/rofi)
- [st](https://st.suckless.org/)
- [neovim](https://github.com/neovim/neovim)
- [firefox](https://www.mozilla.org/en-US/firefox/)
- [cava](https://github.com/karlstav/cava)
- [mps-youtube](https://github.com/mps-youtube/mps-youtube)
- [nnn](https://github.com/jarun/nnn)
- and more...

## Screenshots

![Setup as of 02.2018](https://valcarce.fr/img/rice/02_2018.png)

## Installation

### Dotfiles

To install this setup on a new machine (no exsiting config files):
```bash
git clone --separate-git-dir=~/.dotfiles https://gitlab.com/plut0n/dotfiles ~
```

If you already have existing config file and want to **override** them with the one of this repo (rsync required):
```bash
git clone --separate-git-dir=~/.dotfiles
rsync --recursive --verbose --exclude '.git' tmpdotfiles/ ~
rm -r tmpdotfiles
```

### Dependencies

List of software this setup relies on. Installing them should results in no bug when starting i3:
- `i3-gaps i3lock imagemagick rofi xorg-xset`, they are all available in official or community Archlinux repository.
- `st` is also require, it can be found [here](https://st.suckless.org/) (or, including most common patch, [here](https://github.com/LukeSmithxyz/st)).

Other software having config file in this repo, but no required for i3-gaps to work properly are:
- `firefox neovim mps-youtube nnn` from official Archlinux repos.
- `cava` for AUR or git.

### Other

Neovim relies on vim-plug, that you can install following the instruction [here](https://github.com/junegunn/vim-plug). Then, open neovim and run `:PlugInstall()`

## Colorscheme

Colors for this setup are [pywal](https://github.com/dylanaraps/pywal) compatible. I.e. running `wal -i /path/to/your/wallpaper` will setup colors for the whole setup.

